import React from "react";
import './Header.css';

class Header extends React.Component {
    render() {
        return (
            <header>
                <div className="header-container">
                    <h1>Fake Store</h1>
                    <div className="sub-container">
                        <button>Home</button>
                        <button>SignUp</button>
                    </div>
                </div>
            </header>
        )
    }
}

export default Header;