import React from 'react';
import './App.css';
import Products from './components/Products';
import Header from './components/Header';
import Loader from './components/Loader';
class App extends React.Component {

  constructor(props) {
    super(props);

    this.API_STATES = {
      LOADING: 'loading',
      LOADED: 'loaded',
      ERROR: 'error'
    }

    this.state = {
      products: [],
      status: this.API_STATES.LOADING,
      errorMessage: "",
    }

    this.URL = 'https://fakestoreapi.com/products/'

  }

  fetchData = (url) => {
    this.setState({
      status: this.API_STATES.LOADING,
    }, () => {
      fetch(url)
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          this.setState({
            status: this.API_STATES.LOADED,
            products: data,
          })
        })
        .catch((error) => {
          this.setState({
            status: this.API_STATES.ERROR,
            errorMessage: "An API error occured. please try again in a few minutes",
          })
        })
    })
  }

  componentDidMount = () => {
    this.fetchData(this.URL);
  }

  render() {

    return (
      <div className='main-container'>
        <Header />
        {this.state.status === this.API_STATES.ERROR &&
          <div className='fetch-fail'>
            <h2> {this.state.errorMessage} </h2>
          </div>
        }

        {this.state.status === this.API_STATES.LOADING &&
          <Loader />
        }

        {this.state.status === this.API_STATES.LOADED && this.state.products.length > 0 &&
          <div className='main-container'>
            <ul>
              <Products products={this.state.products} />
            </ul>
          </div>
        }
      </div>
    );
  }

}
export default App;
